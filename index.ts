import { WorkGenerationContract, WorkResultHandler, WebBasedParallelizer, Logger, LogLevel } from "web-based-parallelizer-server";
import webWorkerCode from "./src/WebWorkerCode";
import { WorkCountWorkGenerator } from "./src/WorkCountWorkGenerator";
import { join } from "path";
import { WordCountWorkResultHandler } from "./src/WordCountWorkResultHandler";

Logger.gi().setLevel(LogLevel.INFO);
let sampleName = 'SampleTextFile_1';
let inputFilepath = join(__dirname, 'samples', `${sampleName}.txt`);
let outputFilepath = join(__dirname, 'samples', `${sampleName}_results.txt`);
let workerGenerationContract: WorkGenerationContract = new WorkCountWorkGenerator(10000, inputFilepath);
let workResultHandler: WorkResultHandler = new WordCountWorkResultHandler(outputFilepath);
let aServer = new WebBasedParallelizer(webWorkerCode, workerGenerationContract, workResultHandler);
aServer.start();
let sin = process.stdin;
sin.setEncoding('utf-8');
sin.on('data', (key) => {
    key = key.trim();
    console.log('Stdin: ', key);
    if (key === 'start') {
        console.log('Starting work');
        aServer.startWork();
    }
})
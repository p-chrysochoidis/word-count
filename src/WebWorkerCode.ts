export default `
onmessage = function(workEvent) {
    var work = workEvent.data;
    var lines = work.lines;
    var wordOccuranciesMap = {};
    var wordsWithOccurancies = [];
    var counter;

    lines.forEach(function(line) {
        line.trim().split(" ").forEach(function(aWord) {
            aWord = aWord.trim();
            if (aWord.length) {
                counter = wordOccuranciesMap[aWord] || 0;
                counter++;
                wordOccuranciesMap[aWord] = counter;
            }
        });
    });

    Object.keys(wordOccuranciesMap).forEach(function(aWord) {
        wordsWithOccurancies.push([aWord, wordOccuranciesMap[aWord]]);
    });

    postMessage({
        data: {
            wordsOccurances: wordsWithOccurancies
        },
        work: work
    });
}
`;
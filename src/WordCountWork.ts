import { Work } from "web-based-parallelizer-server";

export class WorkCountWork extends Work {
    constructor(id: string, private lines: string[]) {
        super(id);
    }

    toJson() {
        return { lines: this.lines };
    }
}
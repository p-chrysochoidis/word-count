import { WorkResult, Work } from "web-based-parallelizer-server";

export class WordCountWorkResult extends WorkResult {
    private mWordsOccurrences: [string, number][];

    public get words(): [string, number][] {
        return this.mWordsOccurrences;
    }

    constructor(work: Work, wordOccurrences: [string, number][]) {
        super(work);
        this.mWordsOccurrences = wordOccurrences;
    }
}
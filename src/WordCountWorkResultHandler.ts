import { WorkResultHandler, Work, WorkResult } from "web-based-parallelizer-server";
import { WordCountWorkResult } from "./WordCountWorkResult";
import { openSync, closeSync, writeSync } from "fs";

export class WordCountWorkResultHandler extends WorkResultHandler {

    constructor(private outputFilepath: string) {
        super();
    }

    public allCompletedHandleResults(workResults: WordCountWorkResult[]): void {
        this.logger.info(`All work completed. Total results: ${workResults.length}.`);
        let wordsOccurancesMap: Map<string, number> = new Map();
        let word: string;
        let occurances: number;
        workResults.forEach((result: WordCountWorkResult) => {
            result.words.forEach(wordOccurances => {
                word = wordOccurances[0];
                occurances = wordsOccurancesMap.get(word) || 0;
                occurances += wordOccurances[1];
                wordsOccurancesMap.set(word, occurances)
            });
        });
        let fd: number = null;
        try {
            fd = openSync(this.outputFilepath, 'w');
            wordsOccurancesMap.forEach((occurances, word) => {
                writeSync(fd, `${word} => ${occurances}\n`);
            });
        } catch (error) {
            this.logger.error(`There was an error writting the output file. ${this.outputFilepath}`);
        } finally {
            if (fd !== null) {
                closeSync(fd);
            }
        }
        this.logger.info(`Word count completed. Total unique words: '${wordsOccurancesMap.size}'. Check for output: ${this.outputFilepath}`);
    }

    public desirializeWorkResult(workResultJson: any, work: Work): WorkResult {
        return new WordCountWorkResult(work, workResultJson.wordsOccurances);
    }
}
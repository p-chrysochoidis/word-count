import { WorkGenerationContract, Logger, WorkGenerator, WorkResultHandler } from "web-based-parallelizer-server";
import { WorkCountWork } from "./WordCountWork";
import { readFileSync } from "fs";

export class WorkCountWorkGenerator implements WorkGenerationContract {
    private logger: Logger = Logger.gi();
    private mWorkGenerator: WorkGenerator;

    constructor(private linesPerWork: number, private filePath: string) {
        this.logger.debug(`[WorkCountWorkGenerator] Total lines per work: ${this.linesPerWork}, filepath: ${this.filePath}`);
    }

    public attachWorkGenerator(workGenerator: WorkGenerator): void {
        this.mWorkGenerator = workGenerator;
    }

    public generateWork(): void {
        this.logger.debug(`[WorkCountWorkGenerator] Generating work for workers.`);
        try {
            let workIndex = 0;
            let linesChunk: string[] = [];
            let file = readFileSync(this.filePath, {encoding: "UTF-8"});
            file.split("\n").forEach((line) => {
                line = line.trim();
                if (line.length) {
                    linesChunk.push(line);
                }
                if (linesChunk.length === this.linesPerWork) {
                    this.logger.debug(`Work ready. Creating with chunk of: ${linesChunk.length} lines.`);
                    this.mWorkGenerator.submitWork(new WorkCountWork(`${workIndex++}`, linesChunk));
                    linesChunk = [];
                }
            });
            if (linesChunk.length === this.linesPerWork) {
                this.logger.debug(`Some extra lines left. Creating work with chunk of: ${linesChunk.length} lines.`);
                this.mWorkGenerator.submitWork(new WorkCountWork(`${workIndex++}`, linesChunk));
                linesChunk = [];
            }
        } catch (error) {
            this.logger.error(`Read file error: ${error}`);
        }
        this.mWorkGenerator.allWorkGenerationCompleted();
    }

    public handleCompletedWorkResults(workResults: WorkResultHandler): boolean {
        this.logger.debug(`[WorkCountWorkGenerator] handleCompletedWorkResults - do nothing`);
        //Do nothing
        return false;
    }
}